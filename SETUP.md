# Setup
These are the steps to setup the application

## To run the application
1. Unzip to a folder

2. Start the application with

```bash
python3 main.py

```

3. Execute the fibonacci operation

Make a GET request to http://localhost:8000

```bash

curl  -X GET \
  'http://localhost:8000/n?n=5' \
  --header 'Accept: */*' \
  
```

<br/>




# PS: There are some steps which I implemented and do not thoroughly detail in this setup file, (Those could be discussed verbally in a meeting), for instance

1. I have setup Semantic Release for release management and to generate version number for the application, which is used to tag the docker images built via Gitlab CI pipeline. This helps to control the code version deployed and also aids with rollback of changes. 

2. I have adopted a customized Gitlow strategy for this repo. There are essentially 3 primary branches in the repo, namely, 'master' which builds and deploys to production environment , 'beta' which builds and deploys to staging environment and 'develop' which is the main development branch where active development are done. New Feature branch are branched from the develop branch and ensuing Merge Requests are raised against the develop branch by the developer.

3. I have setup Horizontal Pod Autoscaler (HPA) in the Helm chart template configuration to auto scale up and scale down as per metrics collected on cpu load, memory usage, total number of requests the application is receiving. In an ideal production environment, I will complementary setup Karpenter or Cluster AutoScaler on AWS or any Cloud to augment scaling of infrastructure resources as needed. 

4. I have setup livelines and readiness probe in the deployment yaml to check and ensure application is available and reachable.


# Steps taken to setup the application includes

1. ## Develop the Application
  i. Instrument the application with OpenTelemetry or Prometheus (In this, I used Prometheus because it's much faster to setup and this is quick demo with time constraint )

2. ## Dockerize the application

3. ## Setup Helm Chart to package the application deployment

4. ## Setup the CI (Continuous Integration) Pipeline to automate the deployment (build, test, deploy)
<br/>

5. # Setup Kubernetes Cluster following below steps

<br/>

0. ## Install Kong Ingress Controller

 Run the following command in the terminal.

You must have Helm installed to use Helm

```bash

 helm repo add kong https://charts.konghq.com

 helm repo update

 helm upgrade kong kong/kong -n kong -i --create-namespace --set ingressController.installCRDs=false

```

1. ### Install Cert Manager on the cluster to obtain SSL for the app domain

```bash
 helm repo add jetstack https://charts.jetstack.io
 
 helm repo update
 
 helm install cert-manager jetstack/cert-manager --namespace cert-manager --set installCRDs=true --create-namespace
```


2. ### Install Prometheus on the cluster to collect metrics for the infrastructure resources and application running in the cluster

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm repo update

helm upgrade --install prometheus prometheus-community/kube-prometheus-stack -n monitoring --create-namespace

```


3. ### Install Grafana to visualize metrics collected from prometheus in a nice dashboard with intuitive graphs

```bash
helm repo add grafana https://grafana.github.io/helm-charts

helm repo update

helm upgrade --install grafana grafana/grafana -n monitoring --create-namespace


```


4. ### optionaly Install Loki to aggregate logs, if using AWS, I can use CloudWatch for this.

```bash

helm repo add grafana https://grafana.github.io/helm-charts

helm repo update

helm upgrade --install loki grafana/loki -n monitoring --create-namespace

```

5. ### Configure prometheus and Loki as data source in Grafana
- use http://prometheus-kube-prometheus-prometheus.monitoring.svc.cluster.local:9090/ as url in configuring Prometheus datasource.

- use http://loki-gateway.monitoring.svc.cluster.local:3100/ as the url in configuring the Loki datasource.



6. ## Optionally Install Argo CD and Setup GitOps Declarative Deployment, if desired

```bash

helm repo add argo https://argoproj.github.io/argo-helm

helm repo update

helm upgrade argo-cd argo/argo-cd -i -n argo --create-namespace

```

