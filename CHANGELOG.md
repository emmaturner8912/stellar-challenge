## [1.0.1](https://gitlab.com/emmaturner8912/stellar-challenge/compare/v1.0.0...v1.0.1) (2023-07-30)


### Bug Fixes

* add setup instruction ([2d03da5](https://gitlab.com/emmaturner8912/stellar-challenge/commit/2d03da57aa9dfd13380fa960991f3ed1d492caab))

# 1.0.0 (2023-07-30)


### Bug Fixes

* add fibonacci logi, add deployment config ([b814cc9](https://gitlab.com/emmaturner8912/stellar-challenge/commit/b814cc93e0eaea78ce19dfac4970b49da48239e1))
* add fibonacci logi, add deployment config ([508edff](https://gitlab.com/emmaturner8912/stellar-challenge/commit/508edff583709209fade2909409f536e3320135f))
* add setup instruction ([f354f61](https://gitlab.com/emmaturner8912/stellar-challenge/commit/f354f61c65371b458bcfb5867921d64ced7d98d0))
* add setup instruction ([53db4f5](https://gitlab.com/emmaturner8912/stellar-challenge/commit/53db4f59e489f91303fecd3a615538800d3bd0e7))
* ci ([17152d9](https://gitlab.com/emmaturner8912/stellar-challenge/commit/17152d96208093cae63378ddfbdaae030dbf710b))
* correct dockerfile path ([06a1e5c](https://gitlab.com/emmaturner8912/stellar-challenge/commit/06a1e5c8146680e16ea049bf96a75156e28fbd18))

# [1.0.0-beta.2](https://gitlab.com/emmaturner8912/stellar-challenge/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2023-07-30)


### Bug Fixes

* add setup instruction ([53db4f5](https://gitlab.com/emmaturner8912/stellar-challenge/commit/53db4f59e489f91303fecd3a615538800d3bd0e7))

# 1.0.0-beta.1 (2023-07-30)


### Bug Fixes

* add fibonacci logi, add deployment config ([b814cc9](https://gitlab.com/emmaturner8912/stellar-challenge/commit/b814cc93e0eaea78ce19dfac4970b49da48239e1))
* add fibonacci logi, add deployment config ([508edff](https://gitlab.com/emmaturner8912/stellar-challenge/commit/508edff583709209fade2909409f536e3320135f))
* ci ([17152d9](https://gitlab.com/emmaturner8912/stellar-challenge/commit/17152d96208093cae63378ddfbdaae030dbf710b))
* correct dockerfile path ([06a1e5c](https://gitlab.com/emmaturner8912/stellar-challenge/commit/06a1e5c8146680e16ea049bf96a75156e28fbd18))
