FROM python:3.11.4-bookworm

WORKDIR /app

# FIXME:  COPY and pip install requirement.txt if the file exists and dependencies needed to be installed.

COPY . .

CMD ["python3", "main.py"]
